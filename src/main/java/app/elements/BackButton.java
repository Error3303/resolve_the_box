package app.elements;

import javafx.scene.control.Button;

public class BackButton extends Button{

	public BackButton() {
		this.getStylesheets().add(getClass().getResource("MenuButton.css").toExternalForm());
		this.getStyleClass().add("back_btn");
	}
}
