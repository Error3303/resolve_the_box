package app.elements;

import javafx.scene.image.Image;

public class appUtil {

	public static int window_width = 1000;

	public static int window_height = 650;
	
	public static Image box = new Image("/images/premiereEnquete/boite_ouverte.png");
	public static Image box_hover = new Image("/images/premiereEnquete/boite_ouverte_hover.png");
	
	public static Image inside_box = new Image("/images/interieur_boite.png");
	
	public static Image close = new Image("/images/close.png");
	public static Image close_hover = new Image("/images/close_hover.png");
}
