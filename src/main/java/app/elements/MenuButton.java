package app.elements;

import javafx.scene.control.Button;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

public class MenuButton extends Button {

	public MenuButton(final String name) {
		this.setTextFill(Paint.valueOf("white"));
		this.setTextAlignment(TextAlignment.CENTER);
		this.setText(name);
		this.setFont(Font.font("Apple Chancery", 20));
		this.getStylesheets().add(getClass().getResource("MenuButton.css").toExternalForm());
		this.getStyleClass().add("menu_btn");
	}
}
