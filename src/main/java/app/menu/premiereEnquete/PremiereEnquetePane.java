package app.menu.premiereEnquete;

import java.io.IOException;
import java.net.URL;

import app.elements.BackButton;
import app.elements.appUtil;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class PremiereEnquetePane extends Pane {

	public SimpleBooleanProperty menuClosed;

	private Pane mainPane;

	public PremiereEnquetePane() {
		this.getStylesheets().add(getClass().getResource("/css/app.css").toExternalForm());
		mainPane = new Pane();
		menuClosed = new SimpleBooleanProperty(false);

		initBackButton();

		ImageView imageView = new ImageView();
		imageView.setImage(appUtil.box);
		imageView.hoverProperty().addListener(e -> this.boxHover(imageView, imageView.isHover()));
		imageView.setPickOnBounds(false);
		imageView.setFocusTraversable(false);

		mainPane.getChildren().add(imageView);

		this.getChildren().add(mainPane);

		imageView.setLayoutX(appUtil.window_width / 2 - imageView.getImage().getWidth() / 2);
		imageView.setLayoutY(appUtil.window_height / 2 - imageView.getImage().getHeight() / 2);

		imageView.setOnMousePressed(e -> {
			try {
				// Localisation du fichier FXML.
				URL url = getClass().getResource("game/PremiereEnqueteGame.fxml");
				// Création du loader.
				FXMLLoader fxmlLoader = new FXMLLoader(url);
				// Chargement du FXML.
				Pane root;
				root = (Pane) fxmlLoader.load();
				
				Stage stage = new Stage();
				stage.setScene(new Scene(root, 1000, 650));
				stage.initStyle(StageStyle.UNDECORATED);
				stage.show();
				imageView.setImage(appUtil.box);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		});

	}

	private void initBackButton() {
		BackButton backBtn = new BackButton();
		backBtn.setOnAction(e -> backToMenu());
		mainPane.getChildren().add(backBtn);
	}

	private void boxHover(ImageView imageView, boolean isHover) {
		if (isHover) {
			imageView.setImage(appUtil.box_hover);
		} else {
			imageView.setImage(appUtil.box);
		}
	}

	private void backToMenu() {
		mainPane.setVisible(false);
		this.setVisible(false);
		menuClosed.setValue(true);
	}

	public void show() {
		this.setVisible(true);
		mainPane.setVisible(true);
		menuClosed.setValue(false);
	}
}
