package app.menu.premiereEnquete.game;

import app.elements.appUtil;
import javafx.fxml.FXML;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class PremiereEnqueteGame{

	@FXML
	private Pane mainPane;
	
	@FXML
	private void initialize() {
		ImageView imageView = new ImageView();
		imageView.setFitWidth(appUtil.window_width);
		imageView.setFitHeight(appUtil.window_height);
		imageView.setImage(appUtil.inside_box);
		imageView.setPickOnBounds(false);
		imageView.setFocusTraversable(false);
		mainPane.getChildren().add(imageView);
		
		initElements();
		
	}
	
	private void closeWindow() {
		((Stage)mainPane.getScene().getWindow()).close();
		
	}
	
	private void initElements() {
		addCloseButton();
	}

	private void addCloseButton() {
		ImageView close = new ImageView();
		close.setImage(appUtil.close);
		close.setOpacity(0.6);
		close.hoverProperty().addListener((obs, old, newValue) -> {
			if(newValue) {
				close.setImage(appUtil.close_hover);
			} else {
				close.setImage(appUtil.close);
			}
		});
		close.setLayoutX(appUtil.window_width / 2 - close.getImage().getWidth() / 2);
		close.setLayoutY(20);
		close.setOnMousePressed(e -> this.closeWindow());
		mainPane.getChildren().add(close);		
	}
}
