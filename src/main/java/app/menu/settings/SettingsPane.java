package app.menu.settings;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;

public class SettingsPane extends TabPane{

	public SettingsPane() {
		this.setStyle("-fx-background-color:transparent");
		this.createTabs();
	}
	
	private void createTabs() {
		Tab graphicSettings = this.newTab("Graphique");
		graphicSettings.setContent(createGraphicSettings());
		
		Tab menu2 = new Tab("Menu n°2");
		this.getTabs().add(menu2);
	}
	
	private Pane createGraphicSettings() {
		Pane content = new Pane();
		content.setPrefHeight(650);
		content.setPrefWidth(1000);
		this.addContent(content);
		return content;
	}

	private void addContent(Pane pane) {
		VBox vbox = new VBox();
		
		Label lblResolution = new Label("Resolution :");
		ComboBox<String> cbxResolution = new ComboBox<String>();
		remplissageComboBoxResolution(cbxResolution);
		
		this.addLine(vbox, lblResolution, cbxResolution);
		
		
		Label lblLuminosite = new Label("Luminosité : ");
		Slider slider = new Slider();
		slider.setMajorTickUnit(25);
		slider.valueProperty().addListener((obs, old, newValue)-> System.out.println(slider.getValue()));
		
		this.addLine(vbox, lblLuminosite, slider);
	
		pane.getChildren().add(vbox);
	}
	
	private void addLine( VBox vbox, Label label, Node element) {
		HBox line = new HBox();
		line.setPadding(new Insets(20,0,0,20));
		line.setAlignment(Pos.CENTER);
		label.setTextFill(Paint.valueOf("white"));
		label.setFont(Font.font("Apple Chancery", 25));
		
		line.getChildren().add(label);
		line.getChildren().add(element);
		
		vbox.getChildren().add(line);
	}
	
	private void remplissageComboBoxResolution(ComboBox<String> cbx) {
		cbx.getItems().add("800x520");
		cbx.getItems().add("1000x650");
		cbx.getItems().add("1200x780");
	}
	
	private Tab newTab(String nameMenu) {
		Tab tab = new Tab(nameMenu);
		tab.setClosable(false);
		tab.setStyle("-fx-background-color:white");
		this.getTabs().add(tab);
		return tab;
	}
}
