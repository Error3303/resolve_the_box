package app.menu;

import app.elements.MenuButton;
import app.menu.premiereEnquete.PremiereEnquetePane;
import app.menu.settings.SettingsPane;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class MenuView{

	@FXML
	private Pane pane;
	
	private Pane mainPane;
	
	private SimpleBooleanProperty menuClosed;
	
	private PremiereEnquetePane enquete;
	
	@FXML
	public void initialize() {
		enquete = new PremiereEnquetePane();
		this.mainPane = new Pane();
		menuClosed = new SimpleBooleanProperty(true);
		menuClosed.addListener(e -> this.menuClosed(menuClosed.get()));
		ImageView imageView = new ImageView();
		imageView.setImage(new Image("images/menu/logo.png"));
		imageView.setLayoutX((this.pane.getPrefWidth() / 2) - (imageView.getImage().getWidth() / 2));
		this.mainPane.getChildren().add(imageView);
		this.pane.getChildren().add(mainPane);
		initJavaFXComponents();
	}
	
	private void initJavaFXComponents() {
		VBox buttonsVBox = new VBox(90);
		buttonsVBox.setPadding(new Insets(195,0,0,20));
		MenuButton menuPremiereEnquete = addButtons(buttonsVBox, "Enquête n°1");
		menuPremiereEnquete.setOnAction(e->this.switchToPremiereEnquetePane());
		MenuButton menu2 = addButtons(buttonsVBox, "Enquête n°2");
		menu2.setDisable(true);
		MenuButton menuParametres = addButtons(buttonsVBox, "Paramètres");
		menuParametres.setOnAction(e -> this.switchToSettingsPane());
		
		mainPane.getChildren().add(buttonsVBox);
	}
	
	private MenuButton addButtons(VBox buttonsVBox, final String name) {
		MenuButton btn = new MenuButton(name);
		buttonsVBox.getChildren().add(btn);
		return btn;
	}
	
	private void switchToSettingsPane() {
		System.out.println("Switch to Settings Pane");
		this.mainPane.setVisible(false);
		this.pane.getChildren().add(new SettingsPane());
	}
	
	private void switchToPremiereEnquetePane() {
		if(this.pane.getChildren().contains(enquete)) {
			enquete.show();
		}else {
			System.out.println("Switch to premiereEnquete Pane");
			menuClosed.bind(enquete.menuClosed);
			this.pane.getChildren().add(enquete);
		}
		this.mainPane.setVisible(false);
	}
	
	private void menuClosed(boolean result) {
		if(result) {
			this.mainPane.setVisible(true);
		}
	}
}
